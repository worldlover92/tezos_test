import smartpy as sp
FA2 = sp.io.import_script_from_url("https://smartpy.io/templates/FA2.py")

class NFT(FA2.FA2):
   pass

@sp.add_test(name="tests")
def test():
  mo = sp.test_account("mo")
  admin = sp.address("tz1VEDPgPQG9ACSJqrdZtGQenYsjZbVoVQ2j")
  scenario = sp.test_scenario()
  scenario.h1("Simplest NFT")
  nft = NFT(FA2.FA2_config(non_fungible=True), admin=admin, metadata= sp.big_map({"": sp.utils.bytes_of_string("tezos-storage:content"),"content": sp.utils.bytes_of_string("""{"name": "Simplest NFT", "description": "simplest NFT contract"}""")}))
  scenario += nft
  nft.mint(token_id=0, address=mo.address, amount=1, metadata = sp.map({"": sp.utils.bytes_of_string("ipfs://bafkreihdhgraho5hv5b3nh5njsp5va3tu7cll7ye4qa37yczs5ggoxobc4")})).run(sender=admin)
  sp.add_compilation_target("min_comp", nft)
